public class ATM {
    public static void main(String[] args) {
        int[][] data_atm = {
        // format data_atm: {nomor kartu, pin, saldo}
            {123456, 101010, 10000000},
            {135790, 202020, 5000000},
            {121314, 303030, 7000000},
            {143215, 404040, 2000000}
        };
        int jumlah_uang_di_atm = 3000000;
        /*
        - Buatlah pemrograman dengan menirukan alur penarikan uang di ATM
        - Anggaplah ATM tersebut hanya cuma fungsi untuk menarik uang
        - Positive Case:
            - Akan keluar pesan:
                "ATM mengeluarkan uang Rp. xxxx dan Sisa saldo anda: Rp. xxxx"
        - Negative Case:
            - Akan keluar pesan:
                - "PIN anda tidak valid"
                - "Saldo anda tidak mencukupi"
                - "Jumlah uang di ATM tidak mencukupi"
         */
    }
}